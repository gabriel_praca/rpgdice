
//var React = require('react');
import React from 'react';
//var Text = require('react-native').Text;
//var AppRegistry = require('react-native').AppRegistry;
import { AppRegistry, View, Text, Image, Button } from 'react-native';


export default class App extends React.Component{
  constructor() {
    super()
    this.state = {
       number: 20
    }
  }
  getNumber = () => {
    //Random gera numeros aleatórios entre 0 e 1, com várias casas decimais
    //o multiplicador serve para gerar numeros no intervalo de 0 á X onde X é o multiplicador
    //math.floor serve para arredondar os valores retornados pelo random, deixando eles inteiros
    let ret = Math.floor(Math.random() * 20);
    if(ret == 0){
      this.setState({number: 1});
    }
    else{
      this.setState({number: ret});
    }
  };
  render(){
    return(
      <View style={{alignItems: 'center', marginTop: 200}}>
        <Image 
          style={{width: 200, height: 250, resizeMode: 'stretch'}}
          source={require('./images/d20.png')}/>
        <Text style={
          {paddingTop: 80, 
          fontSize: 50, 
          position: 'absolute',
          }}>{this.state.number}</Text>
        <Button title='rodar' onPress={this.getNumber}/>
      </View>
    );
  }
};

AppRegistry.registerComponent('RPGDice', () => {
  return App
});